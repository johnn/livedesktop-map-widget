##TODO: Provide D-M-S as an alternative to decimal.
##TODO: Ptovide 'Pirate' (D-M-S+fraction) as an alternative to decimal.

# Providers' API options:
thunderforest =
    apikey: 'a5402dd5148a4ecb82aa9eb217cec1f0'

HERE =
    app_id: 'FDixXyTnjcfXbKwVJk4U'
    app_code: 'lxRFxcx5Ti9WhE0UmlS_Ww'


# User's configuration:
config =
    background: "Thunderforest.Pioneer" # "Stamen.Watercolor", "Stamen.TerrainBackground", "Stamen.TonerBackground", "NASAGIBS.ViirsEarthAtNight2012", "Thunderforest.Pioneer", "Thunderforest.Landscape", "CartoDB.DarkMatter"
    labels: false # false, "Stamen.TerrainLabels", "Stamen.TonerLabels"
    lines: false # false, "Stamen.TerrainLines", "Stamen.TonerLines"
    apiopts: thunderforest # false, or API options pecified above.
    zoom: 16
    refreshms: 60000
    showLatLng: true # Display latitude, longitude and accuracy
    showUpdated: false # false. Set to true for debugging etc
    showRadius: true # Display accuracy-circle
    showMarker: false # Display location marker
    glass: true # Display random "glasses", circles of random tile layers.
    lat: -36.840556 # Initial latitude (Auckland, NZ)
    lon: 174.74 # Initial longitude (Auckland, NZ)

pioneer =
    name: "Thunderforest.Pioneer"
    apiopts: thunderforest

nls =
    name: "NLS"
    apiopts: null

mapnik =
    name: "OpenStreetMap.Mapnik"
    apiopts: null

blackandwhite =
    name: "OpenStreetMap.BlackAndWhite"
    apiopts: null

hot =
    name: "OpenStreetMap.HOT"
    apiopts: null

hyddafull =
    name: "Hydda.Full"
    apiopts: null

tonerlite =
    name: "Stamen.TonerLite"
    apiopts: null

watercolor =
    name: "Stamen.Watercolor"
    apiopts: null

worldimagery =
    name: "Esri.WorldImagery"
    apiopts: null

positron =
    name: "CartoDB.Positron"
    apiopts: null

darkmatter =
    name: "CartoDB.DarkMatter"
    apiopts: null

L = require 'leaflet'
require 'leaflet-providers'
require './lib/leaflet.magnifyingglass'
baseLayers = []
overlays = []
bgLayer = false
labelsLayer = false
linesLayer = false
map = false
marker = false
circle = false
magnifyingGlass = false
glassRadius = 250
glassGrowth = 10
geotstamp = 0

if config.background
    bgLayer = L.tileLayer.provider(config.background, config.apiopts)
    bgLayer.setZIndex(1)

if config.labels
    # Hack, because Stamen.TerrainLabels isn't available via leaflet-providers, and it looks *so* good with Stamen.Watercolor!
    if config.labels isnt "Stamen.TerrainLabels"
        labelsLayer = L.tileLayer.provider(config.labels, config.apiopts)
    else
        labelsLayer = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-labels/{z}/{x}/{y}.{ext}', {
            ext: 'png'
        })
    labelsLayer.setZIndex(2)

if config.lines
    linesLayer = L.tileLayer.provider(config.lines, config.apiopts)
    linesLayer.setZIndex(3)

if config.glass
    glassLayers = [
        #L.tileLayer.provider(nls.name, nls.apiopts).setZIndex(4),
        #L.tileLayer.provider(pioneer.name, pioneer.apiopts).setZIndex(4),
        L.tileLayer.provider(mapnik.name, mapnik.apiopts).setZIndex(4),
        L.tileLayer.provider(blackandwhite.name, blackandwhite.apiopts).setZIndex(4),
        L.tileLayer.provider(hot.name, hot.apiopts).setZIndex(4),
        L.tileLayer.provider(hyddafull.name, hyddafull.apiopts).setZIndex(4),
        L.tileLayer.provider(tonerlite.name, tonerlite.apiopts).setZIndex(4),
        L.tileLayer.provider(watercolor.name, watercolor.apiopts).setZIndex(4),
        L.tileLayer.provider(worldimagery.name, worldimagery.apiopts).setZIndex(4),
        L.tileLayer.provider(positron.name, positron.apiopts).setZIndex(4)
        L.tileLayer.provider(darkmatter.name, darkmatter.apiopts).setZIndex(4)
        ]

command: "" # Hack: this seems to be necessary - throws a 'Map container not found' error if commented-out.

refreshFrequency: config.refreshms # ms - 300000ms = 5 minutes

render: (_) -> """
                   <div id=\"ldm-mapid\"></div><div id=\"ldm-positionid\"></div>
               """

afterRender: (domEl) ->
    #console.log 'afterRender: START: ' + new Date().toISOString()
    if not lat?
        lat = config.lat
    if not lon?
        lon = config.lon
    $(domEl).find('#ldm-mapid').height (screen.height - 22) + "px"
    $(domEl).find('#ldm-mapid').width screen.width + "px"
    if config.showLatLng
        $(domEl).find('#ldm-positionid').html """<span>#{lat}&deg;, #{lon}&deg; (&plusmn; 500m)</span>"""
    map = new L.Map("ldm-mapid", {
        center: new L.LatLng(lat, lon)
        zoom: config.zoom
        zoomControl: false
        attributionControl: false
    })
    if bgLayer
        map.addLayer(bgLayer)
    if linesLayer
        map.addLayer(linesLayer)
    if labelsLayer
        map.addLayer(labelsLayer)
    if config.showMarker
        marker = L.marker([lat, lon]).addTo(map)
    if config.showRadius
        circle = L.circle([lat, lon], {
            color: '#440088'
            opacity: 0.2
            fill: false
            radius: 500
        }).addTo(map)
    console.log 'afterRender: END: ' + new Date().toISOString()

update: (output, domEl) ->
    #console.log 'update: START: ' + new Date().toISOString()
    if config.glass
        nLayer = Math.floor(Math.random() * glassLayers.length)
    geolocation.getCurrentPosition (e) =>
        #console.log 'geolocation.getCurrentPosition: START: ' + new Date().toISOString()
        coords = e.position.coords
        [lat, lon] = [coords.latitude, coords.longitude]
        acc = coords.accuracy
        geotstamp = new Date(e.position.timestamp).toISOString()
        if config.showLatLng
            dispLatLng = """<span>
                         #{lat.toFixed(3)}&deg;, #{lon.toFixed(3)}&deg; (&plusmn; #{acc.toFixed(0)}m)<br />"""
            if config.showUpdated
                widgettstamp = new Date().toISOString()
                dispLatLng += """<div><span class=\"ldm-updated\">#{geotstamp} (geolocation)</span><br /><span class=\"ldm-updated\">#{widgettstamp} (widget)</span></div>"""
                if config.glass
                    dispLatLng += """<div><span class=\"ldm-updated\" id=\"ldm-glass-layer-name\"></span></div>"""
            dispLatLng += "</span>"
            $(domEl).find('#ldm-positionid').html dispLatLng
            $(domEl).find('#ldm-glass-layer-name').text nLayer
        latLng = new L.LatLng(lat, lon)
        map.panTo(latLng, config.zoom, {animation: true})
        if config.showMarker
            marker.setLatLng(latLng)
        if config.showRadius
            circle.setLatLng(latLng)
            circle.setRadius(parseInt(acc))
        console.log 'geolocation.getCurrentPosition: lat: ' + lat + '; lon: ' + lon + '; timestamp: ' + geotstamp
        #console.log 'geolocation.getCurrentPosition: END: ' + new Date().toISOString()
    if config.glass
        if magnifyingGlass
            map.removeLayer(magnifyingGlass)
        bounds = map.getBounds()
        glassLon = bounds.getWest() + (Math.random() * (bounds.getEast() - bounds.getWest()))
        glassLat = bounds.getSouth() + (Math.random() * (bounds.getNorth() - bounds.getSouth()))
        glassLatLng = new L.LatLng(glassLat, glassLon)
        glassRadius = parseInt(75 + (Math.random() * 275))
        magnifyingGlass = L.magnifyingGlass({
            layers: [ glassLayers[nLayer] ]
            radius: glassRadius
            zoomOffset: 0
            latLng: glassLatLng
        })
        map.addLayer(magnifyingGlass)
    console.log 'update: END: ' + new Date().toISOString() + '; latest geotstamp: ' + geotstamp

style: """
           @import url('livedesktop-map.widget/node_modules/leaflet/dist/leaflet.css')
           @import url('livedesktop-map.widget/lib/leaflet.magnifyingglass.css')

           div#ldm-mapid
               z-index: -99

           div#ldm-positionid
               position: fixed
               font-family: PTSans-NarrowBold
               font-size: 200%
               color: rgba(203, 75, 22, 0.5)
               text-shadow: 2px 2px 4px rgba(255, 255, 255, 0.7), 2px -2px 4px rgba(255, 255, 255, 0.7), -2px 2px 4px rgba(255, 255, 255, 0.7), -2px -2px 4px rgba(255, 255, 255, 0.7)
               letter-spacing: 1px
               padding: 0.5em
               z-index: -98
               top: 0
               left: 0

           span.ldm-updated
               font-size: smaller
       """
